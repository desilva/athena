#include "TrigT2CaloCosmic/T2CaloCosmic.h"
//#include "TrigT2CaloCosmic/T2CaloCosmicMon.h"
//#include "TrigT2CaloCosmic/T2CaloCosmicSamp1.h"
#include "TrigT2CaloCosmic/CosmicSamp2Fex.h"
//#include "TrigT2CaloCosmic/CosmicSamp1Fex.h"
//#include "TrigT2CaloCosmic/CosmicEmEnFex.h"
#include "TrigT2CaloCosmic/CosmicHadEnFex.h"

DECLARE_COMPONENT( T2CaloCosmic )
//DECLARE_COMPONENT( T2CaloCosmicSamp1 )
DECLARE_COMPONENT( CosmicSamp2Fex )
//DECLARE_COMPONENT( CosmicSamp1Fex )
//DECLARE_COMPONENT( CosmicEmEnFex )
DECLARE_COMPONENT( CosmicHadEnFex )
//DECLARE_COMPONENT( T2CaloCosmicMon )

